from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
import time

#Initialize 
driver = webdriver.Chrome()
#Navigate
url = 'https://emplois.ca.indeed.com/jobs?q=developpeur&l=&from=searchOnHP&vjk=83a4e693a7d393a0'
driver.get(url)
time.sleep(5)
wait = WebDriverWait(driver, 10)
job_posts = []
page = 1
post = 1
while True:
    try:
        print('While True is True...')
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        print('Begin extraction ...')
        #  Extract job posts per page
        print(f'Page: {page} -Post: {post}')
        for card in soup.find_all('div', class_='job_seen_beacon'):
            company = card.find('table', class_='jobCard_mainContent big6_visualChanges').text.strip()
            title = card.find('td', class_='resultContent').text.strip()
            description = card.find('div', class_='css-1m4cuuf e37uo190').text.strip()
            details = card.find_all('h2',class_='jobTitle jobTitle-newJob css-j45z4f eu4oa1w0')
            salary = details[0].find('div',class_='heading6 company_location tapItem-gutter companyInfo').parent.text.strip() if len(details) > 1 else 'N/A'
            contract_type = details[1].find('span',class_='companyName').parent.text.strip() if len(details) > 1 else 'N/A'
            location = details[1].find('div',class_='companyLocation').parent.text.strip() if len(details) > 1 else 'N/A'
            #publication = details[1].find('img',alt='date de publication').parent.text.strip() if len(details) > 1 else 'N/A'
            post += 1
            print(f'Company : {company}\nDescription: {description}\nSalary:{salary}\nContrat:{contract_type}\nLocation:{location}\n')
            job_posts.append({
                'Company':company,
                'Title':title,
                'Description':description,
                'Salary':salary,
                'contrat':contract_type,
                'Location':location,
               # 'Date':publication
            })
    except:
        print('Could not scrap...')

    page +=1
    try:
        button = WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable((By.XPATH,'//a[contains(text(), "Suiv.")]'))
        )
        button.click()
        print(f'Page number {page}')
        time.sleep(8)

    except Exception as e:
        print(f'Error encounterd: {e}')
        break
df=pd.DataFrame(job_posts)
print('Head rows\n----------')
print(df.head())
print('Tail rows\n----------')
print(df.tail())
df.to_csv('job_post_461.csv', index=False)
driver.quit()